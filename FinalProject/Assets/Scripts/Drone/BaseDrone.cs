﻿using System.Collections;
using System.Collections.Generic;
using Drone.Bullet;
using UnityEngine;

namespace Drone
{
    public abstract class BaseDrone : MonoBehaviour
    {
        [SerializeField] protected BaseBullet defaultBullet;
        [SerializeField] protected Transform[] gunPosition;

        public float MaxHealth { get; protected set; }
        public float Health { get; protected set; }
        public float MaxEnergy { get; protected set; }
        public float Energy { get; protected set; }
        public float Speed { get; private set; }
        public BaseBullet Bullet { get; private set; }

        protected void Init(float maxHp,float maxEnergy, float speed, BaseBullet bullet)
        {
            MaxHealth = maxHp;
            MaxEnergy = maxEnergy;
            Speed = speed;
            Bullet = bullet;
        }

        public void SetSpeed(float value)
        {
            Speed = value;
        }

        public abstract void Shoot();
        public abstract void ShootStrong();
        
        
    }
}


