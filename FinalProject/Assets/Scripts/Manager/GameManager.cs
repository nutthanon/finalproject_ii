﻿using System;
using System.Collections.Generic;
using Drone.Enemy;
using Drone.Player;
using Items;
using Items.ItemCollection;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        

        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////

        [Header("Manager")] 
        [SerializeField] private ScoreManager scoreManager;
        
        
        [Header("Player Properties")]
        [SerializeField] public PlayerDrone playerDrone;
        [SerializeField] private float playerDroneMaxHp;
        [SerializeField] private float playerDroneMaxEnergy;
        [SerializeField] private float playerDroneMovementSpeed;
        [SerializeField] public bool isPlayerDead;
        
        [Header("Enemy Properties")]
        [SerializeField] private EnemyDrone enemyDrone;
        [SerializeField] private float enemyDroneMaxHp;
        [SerializeField] private float enemyDroneMaxEnergy;
        [SerializeField] private float enemyDroneMovementSpeed;
        
        [Header("Enemy Spawned")]
        [SerializeField] private int maxNumberOfEnemys = 2;
        [SerializeField] private List<EnemyDrone> enemys = new List<EnemyDrone>();

        [Header("Quest Items")] 
        [SerializeField] private QuestItem questItemPrefab;
        [SerializeField] public int maxQuestItems = 3;
        
        [Header("Heal Items")] 
        [SerializeField] private HealItem healItemPrefab;
        [SerializeField] private int maxHealItems = 4;
        
        [Header("Energy Items")] 
        [SerializeField] private EnergyItem energyItemPrefab;
        [SerializeField] private int maxEnergyItems = 5;

        [Header("In-game Items Object")] 
        [SerializeField] public List<HealItem> ingameHealItems = new List<HealItem>();
        [SerializeField] public List<QuestItem> ingameQuestItems = new List<QuestItem>();
        [SerializeField] public List<EnergyItem> ingameEnergyItems = new List<EnergyItem>();
        
        [Header("Map Prefabs")]
        [SerializeField] private List<GameObject> buildings = new List<GameObject>();
        [SerializeField] private List<GameObject> ingameBuilding = new List<GameObject>();
        
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        
        [SerializeField] public int currentCollectedQuestItems;
        [SerializeField] public int currentNumberOfEnemy;
        [SerializeField] public int currentGameLevel;

        public enum GameFlow
        {
            Menu,
            Start,
            Summary,
            Restart,
            NextLevel
        }
        
        public event Action OnStartGame;
        public event Action OnRestartGame;
        public event Action OnNextLevel;
        public event Action OnBackToMenu;
        public event Action OnShowSummary;

        public GameFlow currentGameFlow;
        public bool isGameStart;
        public bool isPassedLevel;

        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        
        // Unity Method
        private void Awake()
        {
            DontDestroyOnLoad(this);
            DontDestroyOnLoad(playerDrone);

            Debug.Assert(playerDrone != null, "player prefab cannot be null");
            Debug.Assert(enemyDrone != null, "enemy prefab cannot be null");

            OnBackToMenu += GoToMenu;
            OnStartGame += StartGame;
            OnRestartGame += RestartGame;
            OnNextLevel += NextLevel;
            OnShowSummary += GameSummary;

            SetDefaultGame();
            SetGameFlow(GameFlow.Menu);
        }

        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        public void SetDefaultGame()
        {
            if (currentNumberOfEnemy > 0 || currentCollectedQuestItems >= 0 || ingameQuestItems.Count > 0)
            {
                ClearGame();
            }
            
            currentCollectedQuestItems = 0;
            currentNumberOfEnemy = 0;
            maxNumberOfEnemys = 2;
            currentGameLevel = 1;
            isGameStart = false;
            isPlayerDead = false;
            isPassedLevel = false;

            maxQuestItems = 3;
            maxHealItems = 4;
            maxEnergyItems = 5;
            
            scoreManager.SetScore(0);
        }
        
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Set current game flow.
        /// </summary>
        /// <param name="nextFlow"></param>
        public void SetGameFlow(GameFlow nextFlow)
        {
            currentGameFlow = nextFlow;
            
            switch (currentGameFlow)
            {
                case GameFlow.Menu:
                {
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    OnBackToMenu?.Invoke();
                    break;
                }
                
                case GameFlow.Start:
                {
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                    OnStartGame?.Invoke();
                    break;
                }
                
                case GameFlow.Restart:
                {
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                    OnRestartGame?.Invoke();
                    break;
                }
                
                case GameFlow.NextLevel:
                {
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                    OnNextLevel?.Invoke();
                    break;
                }
                
                case GameFlow.Summary:
                {
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    OnShowSummary?.Invoke();
                    break;
                }
            }
        }
        
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        
        public void GoToMenu()
        {
            // Set Game Property
            ClearGame();
            SetDefaultGame();
            // Set Ui
            Manager.Get.uiManager.SetCurretUIState(UIManager.UIState.Menu);
            Manager.Get.soundManager.StopSound(SoundManager.Track.BackgroundMusic1);
        }
        
        public void StartGame()
        {
            // Set Game Properties
            SetDefaultGame();
            // Spawn Map
            SpawnMap();
            // Reset Player
            ResetPlayerDrone();
            // Spawn Enemy
            SpawnEnemyDrone();
            // Spawn Item
            SpawnItem();
            // Set Ui to Game Ui
            Manager.Get.uiManager.SetCurretUIState(UIManager.UIState.Started);
            Manager.Get.soundManager.PlaySound(SoundManager.Track.BackgroundMusic1);
            
            isGameStart = true;
            Debug.Log($"Game Start with : isGameStart : {isGameStart}, isPlayerDead : {isPlayerDead}, isPassedLevel : {isPassedLevel}");
        }

        public void RestartGame()
        {
            //Clear Map
            ClearGame();
            StartGame();
            
            Debug.Log($"Game Restart with : isGameStart : {isGameStart}, isPlayerDead : {isPlayerDead}, isPassedLevel : {isPassedLevel}");
        }

        public void NextLevel()
        {
            // Temp Score
            var tempScore = scoreManager.Score;
            // Clear Map
            ClearGame();
            // Set Score
            scoreManager.SetScore(tempScore);
            // Increase Map Level Properties
            currentGameLevel += 1;
            maxNumberOfEnemys += 1;
            maxQuestItems += 1;

            if (maxHealItems < 10) // Limit Item lower than 10
            {
                maxHealItems += 1;
            }
            
            if(maxEnergyItems < 10)  // Limit Item lower than 10
            {
                maxEnergyItems += 1;
            }
            // Spawn Map
            SpawnMap();
            // Reset Player
            ResetPlayerDrone();
            // Spawn Enemy
            SpawnEnemyDrone();
            // Spawn Item
            SpawnItem();
            // Set Ui to Game Ui
            Manager.Get.uiManager.SetCurretUIState(UIManager.UIState.Started);
            
            isGameStart = true;
            isPassedLevel = false;
            Debug.Log($"Game continue next level with : isGameStart : {isGameStart}, isPlayerDead : {isPlayerDead}, isPassedLevel : {isPassedLevel}");
        }

        public void GameSummary()
        {
            // Set Game Property
            if (isPlayerDead) //-- if player is Dead
            {
                // Temp Score
                var tempScore = scoreManager.Score;
                // Clear Map
                ClearGame();
                // Set Score
                scoreManager.SetScore(tempScore);
                
                isGameStart = false;
                
                // Set Ui
                Manager.Get.uiManager.SetCurretUIState(UIManager.UIState.Summary);
                Manager.Get.soundManager.StopSound(SoundManager.Track.BackgroundMusic1);
            }
            else //-- player is Not Dead
            {
                if (isPassedLevel == false) //-- if player is Not Pass Level
                {
                    Debug.LogError("Player is Not Pass Level > Even Player Not Dead");
                }
                else
                {
                    // Temp Score
                    var tempScore = scoreManager.Score;
                    // Clear Map
                    ClearGame();
                    // Set Score
                    scoreManager.SetScore(tempScore);
                    
                    isGameStart = false;
                    
                    // Set Ui
                    Manager.Get.uiManager.SetCurretUIState(UIManager.UIState.SummaryNextLevel);
                }
            }
            
            Debug.Log($"Game Summary with : isGameStart : {isGameStart}, isPlayerDead : {isPlayerDead}, isPassedLevel : {isPassedLevel}");
        }
        
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        
        private void SpawnItem()
        {
            SpawnQuestItem();
            SpawnHealItem();
            SpawnEnergyItem();
        }
        
        private void SpawnQuestItem()
        {
            for (int i = 0; i < maxQuestItems; i++)
            {
                var position = new Vector3(Random.Range(-80, 80), 2, Random.Range(-80, 80));
                var newItemQuest = Instantiate(questItemPrefab,position,Quaternion.identity);
                newItemQuest.Init("Bomb",10,10,BaseItem.Type.Objective);
                
                //Add Event
                newItemQuest.OnCollected += OnCollectedQuestItem;
                ingameQuestItems.Add(newItemQuest);
            }
        }
        
        private void SpawnHealItem()
        {
            for (int i = 0; i < maxHealItems; i++)
            {
                var position = new Vector3(Random.Range(-80, 80), 2, Random.Range(-80, 80));
                var newItemQuest = Instantiate(healItemPrefab,position,Quaternion.identity);
                newItemQuest.Init("Heal Box",10,10,BaseItem.Type.Objective);
                
                //Add Event
                newItemQuest.OnCollected += OnCollectedHealItem;
                ingameHealItems.Add(newItemQuest);
            }
        }
        
        private void SpawnEnergyItem()
        {
            for (int i = 0; i < maxEnergyItems; i++)
            {
                var position = new Vector3(Random.Range(-80, 80), 2, Random.Range(-80, 80));
                var newItemQuest = Instantiate(energyItemPrefab,position,Quaternion.identity);
                newItemQuest.Init("Energy Box",10,10,BaseItem.Type.Objective);
                
                //Add Event
                newItemQuest.OnCollected += OnCollectedEnergyItem;
                ingameEnergyItems.Add(newItemQuest);
            }
        }
        
        private void OnCollectedQuestItem()
        {
            scoreManager.AddScore(100);
            currentCollectedQuestItems += 1;
            Manager.Get.uiManager.UpdateCurrentQuestItemText();
            
            Manager.Get.soundManager.PlaySound(SoundManager.Track.QuestCollect);
            
            if (currentCollectedQuestItems >= maxQuestItems)
            {
                isPlayerDead = false;
                isPassedLevel = true;
                SetGameFlow(GameFlow.Summary);
            }
        }

        private void OnCollectedHealItem()
        {
            var toRegen = playerDrone.MaxHealth * 10 / 100;
            playerDrone.AddHealth(toRegen);
            scoreManager.SubScore(5); //Cost
            Manager.Get.soundManager.PlaySound(SoundManager.Track.ItemCollect);
        }

        private void OnCollectedEnergyItem()
        {
            var toRegen = playerDrone.MaxEnergy * 40 / 100;
            playerDrone.AddEnergy(toRegen);
            scoreManager.SubScore(2); //Cost
            Manager.Get.soundManager.PlaySound(SoundManager.Track.ItemCollect);
        }

        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        
        private void ResetPlayerDrone()
        {
            var drone = playerDrone;
            
            drone.transform.position = new Vector3(0,4,0);
            
            drone.Init(playerDroneMaxHp,playerDroneMaxEnergy, playerDroneMovementSpeed);

            drone.SetHealth(playerDroneMaxHp);
            drone.SetEnergy(playerDroneMaxEnergy);
            
            playerDroneMaxHp = drone.MaxHealth;
            Manager.Get.uiManager.UpdateHpFill();

            playerDroneMaxEnergy = drone.MaxEnergy;
            Manager.Get.uiManager.UpdateEnergyFill();
            
            // Add Event
            drone.OnExploded += OnPlayerDroneExploded; 
            drone.OnShoot += OnShoot; 
            drone.OnShootStrong += OnShootStrong; 
            drone.OnTakeHit += OnTakeHit;
        }

        private void OnPlayerDroneExploded()
        {
            isGameStart = false;
            isPlayerDead = true;
            isPassedLevel = false;
            
            Manager.Get.soundManager.StopSound(SoundManager.Track.BackgroundMusic1);
            Manager.Get.soundManager.PlaySound(SoundManager.Track.Explode);
            
            SetGameFlow(GameFlow.Summary);
        }

        private void OnShoot()
        {
            Manager.Get.uiManager.UpdateEnergyFill();
            Manager.Get.soundManager.PlaySound(SoundManager.Track.Shoot);
        }
        private void OnShootStrong()
        {
            Manager.Get.uiManager.UpdateEnergyFill();
            Manager.Get.soundManager.PlaySound(SoundManager.Track.Shoot);
        }
        
        private void OnTakeHit()
        {
            Manager.Get.uiManager.UpdateHpFill();
            Manager.Get.soundManager.PlaySound(SoundManager.Track.BulletHit);
        }

        private void SpawnEnemyDrone()
        {
            for (int i = 0; i < maxNumberOfEnemys; i++)
            {
                var position = new Vector3(Random.Range(-70, 70), 4, Random.Range(-70, 70));
                var drone = Instantiate(enemyDrone,position,Quaternion.identity);
                drone.Init(enemyDroneMaxHp,enemyDroneMaxEnergy, enemyDroneMovementSpeed);
                
                //Add Event
                drone.OnExploded += OnEnemyDroneExploded;
                drone.OnShoot += EnemyShoot;
                drone.OnTakeHit += OnTakeHit;
                currentNumberOfEnemy += 1;
                enemys.Add(drone);
            }
            
            Manager.Get.uiManager.UpdateCurrentEnemyText();
        }
        
        private void OnEnemyDroneExploded()
        {
            Manager.Get.soundManager.PlaySound(SoundManager.Track.Explode);
            scoreManager.AddScore(5);
            currentNumberOfEnemy -= 1;
            
            if (currentNumberOfEnemy <= 0)
            {
                isPlayerDead = false;
                isPassedLevel = true;
                SetGameFlow(GameFlow.Summary);
            }
            
            Manager.Get.uiManager.UpdateCurrentEnemyText();
        }
        
        private void EnemyShoot()
        {
            Manager.Get.soundManager.PlaySound(SoundManager.Track.Shoot);
        }

        private Quaternion RandomBuidlingRoation()
        {
            var newRotation = Quaternion.identity;
            var rand = Random.Range(0, 3);
            
            switch (rand)
            {
                case 0:
                {
                    newRotation = Quaternion.Euler(0, Random.Range(0.0f, 0.0f), 0);
                    break;
                }
                case 1:
                {
                    newRotation = Quaternion.Euler(0, Random.Range(0.0f, 90.0f), 0);
                    break;
                }

                case 2:
                {
                    newRotation = Quaternion.Euler(0, Random.Range(0.0f, 180.0f), 0);
                    break;
                }
            }

            return newRotation;
        }
        
        private void SpawnMap()
        {
            //var newRotation = Random.rotation;

            if (currentGameLevel >= 1 && currentGameLevel < 3) // Map Level 1
            {
                var newBuildingOne = Instantiate(buildings[0], Vector3.zero, RandomBuidlingRoation());
                var newBuildingTwo = Instantiate(buildings[1], Vector3.zero, RandomBuidlingRoation());
                var newBuildingThree = Instantiate(buildings[2], Vector3.zero, RandomBuidlingRoation());
            
                ingameBuilding.Add(newBuildingOne);
                ingameBuilding.Add(newBuildingTwo);
                ingameBuilding.Add(newBuildingThree);
                
                RenderSettings.fogColor = new Color(240f/255f,240f/255f,240f/255f,.5f);
            }
            else if (currentGameLevel >= 3 && currentGameLevel < 10)  // Map Level 2
            {
                var newBuildingOne = Instantiate(buildings[3], Vector3.zero, RandomBuidlingRoation());
                var newBuildingTwo = Instantiate(buildings[4], Vector3.zero, RandomBuidlingRoation());
                var newBuildingThree = Instantiate(buildings[5], Vector3.zero, RandomBuidlingRoation());
            
                ingameBuilding.Add(newBuildingOne);
                ingameBuilding.Add(newBuildingTwo);
                ingameBuilding.Add(newBuildingThree);
                RenderSettings.fogColor = new Color(135f/255f,30f/255f,30f/255f,.5f);
            }
        }
        
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        
        private void ClearGame()
        {
            ClearMap();
            ClearEnemy();
            ClearItem();
        }

        private void ClearMap()
        {
            foreach (var build in ingameBuilding)
            {
                Destroy(build.gameObject);
            }
            
            ingameBuilding.Clear();
        }
        
        private void ClearEnemy()
        {
            if (currentNumberOfEnemy <= 0)
            {
                return;
            }
            
            foreach (var enemy in enemys)
            {
                if (enemy != null)
                {
                    Destroy(enemy.gameObject);
                }
            }
            
            enemys.Clear();
            currentNumberOfEnemy = 0;
        }
        
        private void ClearItem()
        {
            //Quest
            foreach (var questItem in ingameQuestItems)
            {
                if (questItem != null)
                {
                    Destroy(questItem.gameObject);
                }
                currentCollectedQuestItems = 0;
            }
            
            //Heal
            foreach (var healItem in ingameHealItems)
            {
                if (healItem != null)
                {
                    Destroy(healItem.gameObject);
                }    
            }
            
            
            //Energy
            foreach (var energyItem in ingameEnergyItems)
            {
                if (energyItem != null)
                {
                    Destroy(energyItem.gameObject);
                }    
            }
			
            ingameHealItems.Clear();
            ingameEnergyItems.Clear();
            ingameQuestItems.Clear();
            currentCollectedQuestItems = 0;
        }
    }
}


