﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByTime : MonoBehaviour
{
    [SerializeField] private float lifeTime;
    private float currentLifeTime;

    private void Awake()
    {
        lifeTime = 2;
    }

    void Update()
    {
        if (currentLifeTime < lifeTime)
        {
            if (currentLifeTime >= lifeTime)
            {
                Destroy(this);
            }
            currentLifeTime += Time.deltaTime;
        }
    }
}
