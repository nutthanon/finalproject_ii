﻿using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class UIManager : MonoBehaviour
    {
        //UI
        [Header("Main UI Panel")]
        [SerializeField] public Transform mainMenuPanel;
        [SerializeField] public Transform gameplayerPanel;
        [SerializeField] public Transform summaryPanel;
        
        [Header("Sub Panel")]
        [SerializeField] private Transform howToPlayPanel;
        //[SerializeField] private Transform notifyPanel;
        
        /*[Header("Notify Message")]
        [SerializeField] private TMP_Text notifyText;*/
        
        [Header("Menu Panel Button")] 
        [SerializeField] private Button startButton;
        [SerializeField] private Button howToPlayButton;

        [Header("How To Play Panel")] 
        [SerializeField] private Button howToPlayCloseButton;
        
        [Header("Summary Panel Button")] 
        [SerializeField] private Button nextLevelButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button backToMenuButton;
        
        [SerializeField] private Button[] quitButtons;


        [Header("TMP Text")] 
        [SerializeField] private TMP_Text currentScoreText;
        [SerializeField] private TMP_Text finalScoreText;
        [SerializeField] private TMP_Text highestScoreText;
        [SerializeField] private TMP_Text currentEnemyText;
        [SerializeField] private TMP_Text currentLevelText;
        [SerializeField] private TMP_Text currentQuestItemText;

        [Header("Bar Fill")] 
        [SerializeField] private Image healthFill;
        [SerializeField] private Image energyhFill;

        [SerializeField] private UIState currentUIState;

        public enum UIState
        {
            Menu,
            Started,
            Summary,
            SummaryNextLevel
        }
        
        private void Awake()
        {
            DontDestroyOnLoad(this);

            // UI
            Debug.Assert(mainMenuPanel != null, "mainMenuPanel cannot be null");
            Debug.Assert(gameplayerPanel != null, "gameplayerPanel cannot be null");
            Debug.Assert(summaryPanel != null, "summaryPanel cannot be null");
            
            // UI Button
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(howToPlayButton != null, "howToPlayButton cannot be null");
            Debug.Assert(howToPlayCloseButton != null, "howToPlayCloseButton cannot be null");

            Debug.Assert(nextLevelButton != null, "nextLevelButton cannot be null");
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(backToMenuButton != null, "backToMenuButton cannot be null");
            
            Debug.Assert(quitButtons != null, "quitButtons cannot be null");

            // TMP_Text
            Debug.Assert(currentScoreText != null, "currentScoreText cannot be null");
            Debug.Assert(finalScoreText != null, "finalScoreText cannot be null");
            Debug.Assert(highestScoreText != null, "highestScoreText cannot be null");
            Debug.Assert(currentEnemyText != null, "currentEnemyText cannot be null");
            Debug.Assert(currentLevelText != null, "currentLevelText cannot be null");
            Debug.Assert(currentQuestItemText != null, "currentQuestItemText cannot be null");

            // Add Listener to button
            startButton.onClick.AddListener(OnStartButtonClicked);
            howToPlayButton.onClick.AddListener(OnHowToPlayButtonClicked);
            howToPlayCloseButton.onClick.AddListener(OnHowToPlayCloseButtonClicked);

            nextLevelButton.onClick.AddListener(OnNextLevelRestartButtonClicked);
            restartButton.onClick.AddListener(OnRestartButtonClicked);
            backToMenuButton.onClick.AddListener(OnBackToMenuClicked);

            foreach (var quitButton in quitButtons)
            {
                quitButton.onClick.AddListener(OnQuitButtonClicked);
            }

            UpdateNextButton();
        }
        
        // Button
        private void OnBackToMenuClicked()
        {
            Manager.Get.gameManager.SetGameFlow(GameManager.GameFlow.Menu);
        }
        
        private void OnStartButtonClicked()
        {
            Manager.Get.gameManager.SetGameFlow(GameManager.GameFlow.Start);
        }

        private void OnHowToPlayButtonClicked()
        {
            howToPlayPanel.gameObject.SetActive(true);
        }
        private void OnHowToPlayCloseButtonClicked()
        {
            howToPlayPanel.gameObject.SetActive(false);
        }
        
        private void OnRestartButtonClicked()
        {
            Manager.Get.gameManager.SetGameFlow(GameManager.GameFlow.Restart);
        }
        
        private void OnNextLevelRestartButtonClicked()
        {
            Manager.Get.gameManager.SetGameFlow(GameManager.GameFlow.NextLevel);
        }

        private void OnQuitButtonClicked()
        {
            /*if (EditorApplication.isPlaying)
            {
                EditorApplication.isPlaying = false;
            }*/

            Application.Quit();
        }

        //Update UI Method
        public void SetCurretUIState(UIState state)
        {
            currentUIState = state;

            switch (currentUIState)
            {
                case UIState.Menu:
                {
                    ShowMenu();
                    break;
                }

                case UIState.Started:
                {
                    ShowGameUi();
                    break;
                }

                case UIState.Summary:
                {
                    ShowSummary();
                    break;
                }

                case UIState.SummaryNextLevel:
                {
                    ShowSummaryNextLevel();
                    break;
                }
            }
        }

        public void ShowMenu()
        {
            mainMenuPanel.gameObject.SetActive(true);
            gameplayerPanel.gameObject.SetActive(false);
            summaryPanel.gameObject.SetActive(false);
        }

        public void ShowGameUi()
        {
            UpdateScoreText();
            UpdateHpFill();
            UpdateEnergyFill();
            UpdateCurrentEnemyText();
            UpdateCurrentLevelText();
            UpdateCurrentQuestItemText();
            
            mainMenuPanel.gameObject.SetActive(false);
            gameplayerPanel.gameObject.SetActive(true);
            summaryPanel.gameObject.SetActive(false);
        }
        
        public void ShowSummary()
        {
            mainMenuPanel.gameObject.SetActive(false);
            gameplayerPanel.gameObject.SetActive(false);
            summaryPanel.gameObject.SetActive(true);

            if (Manager.Get.scoreManager.Score >= Manager.Get.scoreManager.HighestScore )
            {
                Manager.Get.scoreManager.SetHighestScore(Manager.Get.scoreManager.Score);
            }

            UpdateNextButton();
            
            UpdateSummaryText();
        }

        public void ShowSummaryNextLevel()
        {
            mainMenuPanel.gameObject.SetActive(false);
            gameplayerPanel.gameObject.SetActive(false);
            summaryPanel.gameObject.SetActive(true);

            UpdateNextButton();
            
            if (Manager.Get.scoreManager.Score >= Manager.Get.scoreManager.HighestScore)
            {
                Manager.Get.scoreManager.SetHighestScore(Manager.Get.scoreManager.Score);
            }
            
            UpdateSummaryText();
        }
        
        
        // Update Ui Method
        private void UpdateNextButton()
        {
            if (Manager.Get.gameManager.isPassedLevel && Manager.Get.gameManager.isPlayerDead == false)
            {
                nextLevelButton.gameObject.SetActive(true);
            }
            else
            {
                nextLevelButton.gameObject.SetActive(false);
            }
        }
        
        public void UpdateHpFill()
        {
            healthFill.fillAmount = Manager.Get.gameManager.playerDrone.Health / Manager.Get.gameManager.playerDrone.MaxHealth;
        }

        public void UpdateEnergyFill()
        {
            energyhFill.fillAmount = Manager.Get.gameManager.playerDrone.Energy / Manager.Get.gameManager.playerDrone.MaxEnergy;
        }
        
        public void UpdateCurrentEnemyText()
        {
            currentEnemyText.text = $"Enemy {Manager.Get.gameManager.currentNumberOfEnemy}";
        }
        
        public void UpdateCurrentLevelText()
        {
            currentLevelText.text = $"Level {Manager.Get.gameManager.currentGameLevel}";
        }
        
        public void UpdateCurrentQuestItemText()
        {
            currentQuestItemText.text = $"Objective\n{Manager.Get.gameManager.currentCollectedQuestItems} / {Manager.Get.gameManager.maxQuestItems}";
        }
        
        public void UpdateScoreText()
        {
            currentScoreText.text = $"Score {Manager.Get.scoreManager.Score}";
        }

        public void UpdateSummaryText()
        {
            finalScoreText.text = $"{Manager.Get.scoreManager.Score}";
            highestScoreText.text = $"{Manager.Get.scoreManager.HighestScore}";
        }
    }
}
