﻿using System;
using Drone.Player;
using Manager;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Drone.Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemyDrone enemyDrone;

        // Chasing
        private Transform playerDrone;
        float MaxDist = 10;
        float MinDist = 5;
        
        private float fireRate = 2.0f;
        private float fireCounter;
        
        private bool isStuckInOtherEnvironmentObject;
        private bool isShootAble;

        private void Awake()
        {
            playerDrone = GameObject.FindWithTag("Player").transform;
        }

        private void Update()
        {
            if (fireCounter < fireRate)
            {
                if (fireCounter >= fireRate)
                {
                    isShootAble = true;
                    fireCounter = 0;
                }
                fireCounter += Time.deltaTime;
            }

            if (isStuckInOtherEnvironmentObject)
            {
                RandomNewOriginPosition();
            }

            if (isStuckInOtherEnvironmentObject == false && Manager.Manager.Get.gameManager.isPassedLevel == false && Manager.Manager.Get.gameManager.isGameStart == true && Manager.Manager.Get.gameManager.isPlayerDead == false)
            {
                MoveToPlayer();
            }
        }

        private void MoveToPlayer()
        {

            transform.LookAt(playerDrone);

            if (Vector3.Distance(transform.position, playerDrone.position) >= MinDist)
            {

                transform.position += transform.forward * enemyDrone.Speed * Time.deltaTime;
                

                if (Vector3.Distance(transform.position, playerDrone.position) <= MaxDist)
                {
                    if (isShootAble)
                    {
                        enemyDrone.Shoot();
                        isShootAble = false;
                    }
                }
                else
                {
                    isShootAble = false;
                }
            }
        }

        public void RandomNewOriginPosition()
        {
            var randomX = Random.Range(-60, 60);
            var randomY = Random.Range(-60, 60);
            var newPosition = new Vector3(randomX,4,randomY);
            transform.position = newPosition;
            isStuckInOtherEnvironmentObject = false; 
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("PlayerBullet"))
            {
                var target = enemyDrone.gameObject.GetComponent<IDamagable>();
                target?.TakeHit(Manager.Manager.Get.gameManager.playerDrone.GetBulletDamage());
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                return;
            }
            
            if (other.CompareTag($"Environment/Other"))
            {
                isStuckInOtherEnvironmentObject = true;
            }
        }
    }
}