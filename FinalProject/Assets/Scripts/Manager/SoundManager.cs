﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Serialization;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        [Serializable]
        public struct Soundtrack
        {
            public Track track;
            public TrackType type;
            public AudioClip clip;
        }
        
        public enum TrackType
        {
            MusicBackground,
            SoundEffect
        }
        
        public enum Track
        {
            BackgroundMusic1,
            Shoot,
            Explode,
            QuestCollect,
            ItemCollect,
            BulletHit
        }

        [SerializeField] public Soundtrack[] audioClips;
        [SerializeField] private AudioSource audioSourceSfx;
        [SerializeField] private AudioSource audioSourceBgm;
        [SerializeField] private bool isPlayLoop = true;

        [Range(0.0f, 1.0f)] public float sfxVolume = .2f;
        [Range(0.0f, 1.0f)] public float bgmVolume = .2f;
        
        private float sfxVolumeTemp;
        private float bgmVolumeTemp;

        private void Awake()
        {
            DontDestroyOnLoad(this);
 
            if (sfxVolume < 0.1f)
            {
                sfxVolume = .2f;
            }

            if (bgmVolume < 0.1f)
            {
                bgmVolume = .2f;
            }
        }

        private void Update()
        {
            if (bgmVolumeTemp !=  bgmVolume || sfxVolumeTemp != sfxVolume)
            {
                UpdateVolumn();
            }
        }

        private void UpdateVolumn()
        {
            audioSourceSfx.volume = sfxVolume;
            audioSourceBgm.volume = bgmVolume;
            
            sfxVolumeTemp = sfxVolume;
            bgmVolumeTemp = bgmVolume;
        }

        public void PlaySound(Track track)
        {
            if (audioClips.Length > 0)
            {
                for (int i = 0; i < audioClips.Length; i++)
                {
                    if (audioClips[i].track == track)
                    {
                        switch (audioClips[i].type)
                        {
                            case TrackType.MusicBackground:
                            {
                                PlayBgm(audioClips[i]);
                                break;
                            }
                            case TrackType.SoundEffect:
                            {
                                PlaySfx(audioClips[i]);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                Debug.Log("Audio Clips Length not more than zero.");
            }
        }
        
        public void StopSound(Track track)
        {
            if (audioClips.Length > 0)
            {
                for (int i = 0; i < audioClips.Length; i++)
                {
                    if (audioClips[i].track == track)
                    {
                        switch (audioClips[i].type)
                        {
                            case TrackType.MusicBackground:
                            {
                                audioSourceBgm.Stop();
                                break;
                            }
                            case TrackType.SoundEffect:
                            {
                                audioSourceSfx.Stop();
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                Debug.Log("Audio Clips Length not more than zero.");
            }
        }

        private void PlaySfx(Soundtrack soundtrack)
        {
            audioSourceSfx.volume = sfxVolume;
            audioSourceSfx.PlayOneShot(soundtrack.clip);
        }

        private void PlayBgm(Soundtrack soundtrack)
        {
            audioSourceBgm.clip = soundtrack.clip;
            audioSourceBgm.volume = bgmVolume;
            audioSourceBgm.loop = isPlayLoop;
            audioSourceBgm.Play();

        }
    }
}