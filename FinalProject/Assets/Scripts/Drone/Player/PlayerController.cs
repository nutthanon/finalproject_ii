﻿using System;
using Items;
using Items.ItemCollection;
using Manager;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;

namespace Drone.Player
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerDrone playerDrone;
        [SerializeField] private CharacterController controller;

        // Animation
        [SerializeField] private Animator animator;
        private string currentState;
        private const string PLAYER_OPEN = "Open";
        private const string PLAYER_IDLE = "Idle";
        private const string PLAYER_MOVEFORWARD = "Move_Forward";
        private const string PLAYER_MOVEFUP = "Move_Up";
        private const string PLAYER_SHOOT = "Shoot";
        private const string PLAYER_SHOOTSTORNG = "ShootStrong";
        private const string PLAYER_SEARCH = "Search";


        // Input System
        private DroneInputActions inputActions;
        private Transform cameraMainTransform;
        private Vector3 movementInput = Vector3.zero;
        private bool isMovingUp;
        private bool isMovingDown;
        
        [SerializeField] private float rotationSpeed = 4f;
        [SerializeField] private float jumpHeight = 1.0f;
        [SerializeField] private float gravityValue = -9.81f;
        
        private Vector3 playerVelocity;
        private bool groundedPlayer;
        private bool isMoveAble;
        private bool isBoost;

        private GameObject nearestItem;
        private bool collectable = false;
        private static readonly int IsMoving = Animator.StringToHash("isMoving");
        private static readonly int IsMovingUp = Animator.StringToHash("isMovingUp");
        private static readonly int IsShooting = Animator.StringToHash("isShooting");
        private static readonly int IsShootingStrong = Animator.StringToHash("isShootingStrong");
        private static readonly int IsSeaching = Animator.StringToHash("isSeaching");
        private static readonly int IsStarted = Animator.StringToHash("isStarted");

        private void Awake()
        {
            InitInput();
            isMoveAble = true;
            isBoost = false;
            animator = GetComponent<Animator>();
        }

        private void Start()
        {
            controller = GetComponent<CharacterController>();
            cameraMainTransform = Camera.main.transform;
            ChangeAnimationState(PLAYER_OPEN);
            animator.SetBool(IsStarted, true);
        }

        private void Update()
        {
            Move();

            if (Manager.Manager.Get.gameManager.isGameStart)
            {
                animator.SetBool(IsStarted, true);
            }
            else
            {
                animator.SetBool(IsStarted, false);
            }
        }

        // Method
        private void InitInput()
        {
            inputActions = new DroneInputActions();
            
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Interact.performed += OnInteract;
            inputActions.Player.Interact.canceled += OnInteract;
            inputActions.Player.Up.performed += OnMoveUp;
            inputActions.Player.Up.canceled += OnMoveUp;
            inputActions.Player.Shoot.performed += OnShoot;
            inputActions.Player.Shoot.canceled += OnShoot;
            inputActions.Player.ShootStrong.performed += OnShootStrong;
            inputActions.Player.ShootStrong.canceled += OnShootStrong;
            inputActions.Player.Boost.performed += OnBoost;
            inputActions.Player.Boost.canceled += OnBoost;

            isMovingUp = false;
            isMovingDown = false;
        }
        
        // New Input System
        private void OnInteract(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                isMoveAble = false;
                
                animator.SetBool(IsSeaching, true);
                if (context.interaction is HoldInteraction)
                {
                    Interact();
                }
            }

            if (context.canceled)
            {
                animator.SetBool(IsSeaching, false);
                isMoveAble = true;
                ChangeAnimationState(PLAYER_IDLE);
            }
            
        }

        private void OnMoveUp(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                animator.SetBool(IsMoving, false);
                animator.SetBool(IsMovingUp, true);
                if (playerDrone.transform.position.y < 16)
                {
                    ChangeAnimationState(PLAYER_MOVEFUP);
                    playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
                }
            }

            if (context.canceled)
            {
                animator.SetBool(IsMovingUp, false);
                ChangeAnimationState(PLAYER_IDLE);
                isMovingUp = false;
            }
        }

        private void OnShoot(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                if (playerDrone.Energy >= 0 && GetIsControlable())
                {
                    animator.SetBool(IsShooting, true);
                    ChangeAnimationState(PLAYER_SHOOT);
                    playerDrone.Shoot();
                }
            }

            if (context.canceled)
            {
                animator.SetBool(IsShooting, false);
                ChangeAnimationState(PLAYER_IDLE);
            }
            
        }
        
        private void OnShootStrong(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                if (playerDrone.Energy >= 0 && GetIsControlable())
                {
                    isMoveAble = false;
                    animator.SetBool(IsShootingStrong, true);
                    ChangeAnimationState(PLAYER_SHOOTSTORNG);
                    playerDrone.ShootStrong();
                }
            }

            if (context.canceled)
            {
                isMoveAble = true;
                animator.SetBool(IsShootingStrong, false);
                ChangeAnimationState(PLAYER_IDLE);
            }
        }
        
        public void OnMove(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                movementInput = context.ReadValue<Vector2>();
            }

            if (context.canceled)
            {
                
                movementInput = Vector2.zero;
            }
        }
        
        public void OnBoost(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                if (context.interaction is HoldInteraction)
                {
                    isBoost = true;
                }
            }

            if (context.canceled)
            {
                isBoost = false;
            }
        }

        public bool GetIsControlable()
        {
            return Manager.Manager.Get.gameManager.isPlayerDead == false && 
                   Manager.Manager.Get.gameManager.isPassedLevel == false &&
                   Manager.Manager.Get.gameManager.isGameStart == true;
        }
        
        // Player Movement Method
        private void Move()
        {
            if (isMoveAble && GetIsControlable())
            {
                if (movementInput.x != 0 && movementInput.y != 0 && movementInput.z != 0)
                {
                    animator.SetBool(IsMoving, true);
                    ChangeAnimationState(PLAYER_MOVEFORWARD);
                }
                else
                {
                    animator.SetBool(IsMoving, false);
                    ChangeAnimationState(PLAYER_IDLE);
                }
                
                groundedPlayer = controller.isGrounded;
                if (groundedPlayer && playerVelocity.y < 0)
                {
                    playerVelocity.y = 0f;
                }

                Vector2 movement = movementInput;
                Vector3 move = new Vector3(movement.x, 0, movement.y);
                move = cameraMainTransform.forward * move.z + cameraMainTransform.right * move.x;
                move.y = 0f;
            
                

                // Changes the height position of the player..
                /*if (isMovingUp && groundedPlayer)
                {
                    playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
                }*/
                
                controller.Move(playerVelocity * Time.deltaTime);
                if (isBoost == false)
                {
                    controller.Move(move * Time.deltaTime * playerDrone.Speed);
                }
                else
                {
                    if (playerDrone.Energy > 0)
                    {
                        controller.Move(move * Time.deltaTime * (playerDrone.Speed * 1.5f));
                        playerDrone.SubEnergy(.2f);
                    }
                    else
                    {
                        isBoost = false;
                    }
                }

                
                playerVelocity.y += gravityValue * Time.deltaTime; // G

                if (movement != Vector2.zero)
                {
                    float targetAngle = Mathf.Atan2(movement.x, movement.y) * Mathf.Rad2Deg + cameraMainTransform.eulerAngles.y;

                    Quaternion rotation = Quaternion.Euler(0f, targetAngle, 0f);
                    transform.rotation = Quaternion.Lerp(transform.rotation,rotation,Time.deltaTime * rotationSpeed);
                }
            }
        }

        private void Interact()
        {
            if (nearestItem == null)
            {
                return;
            }
            
            if (collectable == true)
            {
                isMoveAble = false;
                ChangeAnimationState(PLAYER_SEARCH);
                var target = nearestItem.gameObject.GetComponent<ICollectable>();
                target?.Collected();
                Destroy(nearestItem.gameObject);
                nearestItem = null;
                collectable = false;

            }
        }
        
        // Animation Method
        private void ChangeAnimationState(string newState)
        {
            if(playerDrone.gameObject.activeSelf == false) return;
            if(currentState == newState) return;

            // Play
            animator.Play(newState);
            
            // Reassign
            currentState = newState;
        }
        
        // Other Method
        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag($"Items/Bomb"))
            {
                if (nearestItem == null)
                {
                    nearestItem = other.gameObject;
                    Debug.Log("Nearest Item : " + nearestItem.name);
                    collectable = true;
                }
            }

            if (other.CompareTag($"Items/Heal"))
            {
                if (playerDrone.Health >= playerDrone.MaxHealth)
                {
                    return;
                }

                var target = other.gameObject.GetComponent<ICollectable>();
                target?.Collected();
                
                Destroy(other.gameObject);
            }
            
            if (other.CompareTag($"Items/Battery"))
            {
                if (playerDrone.Energy >= playerDrone.MaxEnergy)
                {
                    return;
                }

                var target = other.gameObject.GetComponent<ICollectable>();
                target?.Collected();
                
                Destroy(other.gameObject);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag($"Items/Bomb"))
            {
                nearestItem = null;
                collectable = false;
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("ElectricField"))
            {
                playerDrone.TakeHit(0.5f);
            }
        }
    }
}