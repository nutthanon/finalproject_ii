﻿using System;
using Drone.Player;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Items
{
    public abstract class BaseItem : MonoBehaviour
    {

        public enum Type
        {
            Objective,
            Heal,
            Energy
        }
        
        public string Name { get; protected set; }
        public float Weight { get; protected set; }
        public float Cost { get; protected set; }
        public Type itemType;
        private bool isStuckInOtherEnvironmentObject;

        private void Awake()
        {
            RandomNewOriginPosition();
        }

        public abstract void Init(string name, float weight, float cost, Type type);

        private void Update()
        {
            if (isStuckInOtherEnvironmentObject)
            {
                RandomNewOriginPosition();
            }
        }

        public void RandomNewOriginPosition()
        {
            var randomX = Random.Range(-100, 100);
            var randomY = Random.Range(-100, 100);
            var newPosition = new Vector3(randomX,2,randomY);
            transform.position = newPosition;
            isStuckInOtherEnvironmentObject = false; 
        }

        

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag($"Environment/Other") || other.gameObject.CompareTag("Items/Battery") || other.gameObject.CompareTag("Items/Bomb") || other.gameObject.CompareTag("Items/Heal"))
            {
                isStuckInOtherEnvironmentObject = true;
            }
        }

        private void OnCollisionStay(Collision other)
        {
            if (other.gameObject.CompareTag($"Environment/Other") || other.gameObject.CompareTag("Items/Battery") || other.gameObject.CompareTag("Items/Bomb") || other.gameObject.CompareTag("Items/Heal"))
            {
                isStuckInOtherEnvironmentObject = true;
            }
        }
        
        /*private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag($"Environment/Other") || other.CompareTag("Items/Battery") || other.CompareTag("Items/Bomb") || other.CompareTag("Items/Heal"))
            {
                isStuckInOtherEnvironmentObject = true;
            }
        }*/

        /*private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                return;
            }
            
            if (other.CompareTag($"Environment/Other") || other.CompareTag("Items/Battery") || other.CompareTag("Items/Bomb") || other.CompareTag("Items/Heal"))
            {
                isStuckInOtherEnvironmentObject = true;
            }
        }*/
    }
}