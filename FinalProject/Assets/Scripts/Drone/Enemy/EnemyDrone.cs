﻿using System;
using UnityEngine;

namespace Drone.Enemy
{
    public class EnemyDrone : BaseDrone, IDamagable
    {
        public event Action OnExploded;
        public event Action OnShoot;
        public event Action OnTakeHit;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "Enemy Bullet cannot be null");
            Debug.Assert(gunPosition != null, "Enemy gunPosition cannot be null");
        }
        
        public void Init(float maxHp,float maxEnergy, float speed)
        {
            base.Init(maxHp,maxEnergy, speed, defaultBullet);
        }

        public void TakeHit(float damage)
        {
            Health -= damage;
            OnTakeHit?.Invoke();
            if (Health > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Health <= 0,"Hp is more than zero");
            //gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Shoot()
        {
            if (Energy <= 0)
            {
                return;
            }
            
            OnShoot?.Invoke();
            var bulletCenter = Instantiate(defaultBullet, gunPosition[0].position, Quaternion.identity);
            bulletCenter.Init();   
        }

        public override void ShootStrong()
        {
             
        }
    }
}