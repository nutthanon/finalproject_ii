 # README #
This README would normally document to share information about this project.

### THIS PROJECT IS WORK-IN-PROGRESS (WIP) CONTAIN LATEST VERSION OF UPDATE IN GAME DEVELOP FLOW ###

* This project is a Final Project an part of subject GI242
* Using for Final Project presentation of subject GI242
 
 ### What is this repository for? ###

* For management files and control version of project and make backup.
* For keeping this repository as portfolio for group members
* [University BU](https://www.bu.ac.th/th/it-innovation/games-and-interactive-media)
 
 ### How do I get set up? ###

* Unity Engine
* Packages : Input System
* Packages : Cinemachine (v2.6.4 March 26, 2021)
* Packages : 2D Sprite (v1.0.0 March 03, 2021)

### Goal of this project ###

* Game flow > Start > Load Scene > Menu > Show summary
* Game Levels more than 1 levels
* Using Code convention, Structure, No Error at Run Time
* Game UI
 
### Members of this project ###
 
* 1.  [1620703916](yingwat.wong@bumail.net)
	-- ยิ่งวัฒน์ ว่องไววิศวกุล
	-- Section 2522 No. 16
* 2. [1620704070](nutthanon.pats@bumail.net)
	-- ณัฐนนท์ แพทย์สุวรรณ
    -- Section 2522 No. 17
* 3. [1620705648](saruntun.yimy@bumail.net)
	-- สรัญชทัน ยิ้มอยู่
    -- Section 2522 No. 22
* 4. [1620707123](Tanapon.@bumail.net) 
	-- ธนพล กอนกุล
    -- Section 2522 No. 28

=========================================    :cherry_blossom:    =========================================