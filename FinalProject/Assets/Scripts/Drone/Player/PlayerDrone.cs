﻿using System;
using Drone.Bullet;
using Items;
using Manager;
using UnityEngine;

namespace Drone.Player
{
    public class PlayerDrone : BaseDrone, IDamagable
    {
        public event Action OnExploded;
        public event Action OnShoot;
        public event Action OnShootStrong;
        public event Action OnTakeHit;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }

        public void Init(float maxHp,float maxEnegy, float speed)
        {
            base.Init(maxHp,maxEnegy, speed, defaultBullet);
        }


        public void SetHealth(float value)
        {
            Health = value;
        }
        
        public void AddHealth(float value)
        {
            if (Health + value >= MaxHealth)
            {
                SetHealth(MaxHealth);
            }
            else
            {
                Health += value;
            }
            
            Manager.Manager.Get.uiManager.UpdateHpFill();
            
           
        }
        
        public void SetEnergy(float value)
        {
            Energy = value;
        }

        public void AddEnergy(float value)
        {
            if (Energy + value >= MaxEnergy)
            {
                SetEnergy(MaxEnergy);
            }
            else
            {
                Energy += value;
            }
            
            Manager.Manager.Get.uiManager.UpdateEnergyFill();
        }

        public void SubEnergy(float value)
        {
            if (Energy < 0)
            {
                Energy = 0;
            }

            else
            {
                Energy -= value;
            }
            
            Manager.Manager.Get.uiManager.UpdateEnergyFill();
        }

        public float GetBulletDamage()
        {
            return base.Bullet.GetDamage();
        }
        
        public override void Shoot()
        {
            if (Energy <= 0)
            {
                return;
            }

            if (Energy >= 5)
            {
                Energy -= 5;
                
                OnShoot?.Invoke();
                var bulletCenter = Instantiate(defaultBullet, gunPosition[0].position, transform.rotation);
                bulletCenter.Init();
            }
        }

        public override void ShootStrong()
        {
            if (Energy <= 0)
            {
                return;
            }

            if (Energy >= 20)
            {
                Energy -= 20;
                OnShootStrong?.Invoke();
                var bulletCenter = Instantiate(defaultBullet, gunPosition[0].position, transform.rotation);
                bulletCenter.Init();
            }
        }
        
        public void TakeHit(float damage)
        {
            Health -= damage;
            
            OnTakeHit?.Invoke();
            
            if (Health > 0)
            {
                return;
            }
            
            Explode();
        }
        
        // Dead
        public void Explode()
        {
            Debug.Assert(Health <= 0,"Hp is more than zero");
            //Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}