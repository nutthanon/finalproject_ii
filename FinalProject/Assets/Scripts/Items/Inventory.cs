﻿//TODO: Implement this later (un-finish class)

using System.Collections.Generic;
using Items.ItemCollection;
using UnityEngine;

namespace Items
{
    public class Inventory : MonoBehaviour
    {
        [SerializeField] private List<QuestItem> questItemsInventory;

        public void AddQuestItem(QuestItem item)
        {
            questItemsInventory.Add(item);
        }
        
        public int GetQuestInventoryCount()
        {
            return questItemsInventory.Count;
        }
    }
}