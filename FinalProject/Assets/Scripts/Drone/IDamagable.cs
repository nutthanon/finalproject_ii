﻿using System;

namespace Drone
{
    public interface IDamagable
    {
        event Action OnExploded;
        void TakeHit(float damage);
        void Explode();
    }
}