﻿using UnityEngine;

namespace Manager
{
    public class Manager : MonoBehaviour
    {
        [Header("Manager")]
        [SerializeField] public GameManager gameManager;
        [SerializeField] public UIManager uiManager;
        [SerializeField] public ScoreManager scoreManager;
        [SerializeField] public SoundManager soundManager;
        
        public static Manager Get{ set; get;} // Instance
        public int value;

        private void Awake()
        {
            if (Get == null)
            {
                Get = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
            
            DontDestroyOnLoad(this);
        }
    }
}