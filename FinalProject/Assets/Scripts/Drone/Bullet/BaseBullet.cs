﻿using System;
using Drone.Enemy;
using UnityEngine;

namespace Drone.Bullet
{
    public class BaseBullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private Rigidbody bulletRigidbody;
        [SerializeField] private GameObject hitEffect;
        [SerializeField] private float destroyTime = 5;

        private Vector2 bulletDirection;
        
        public event Action OnBulletHit;
        public event Action OnBulletSpawn;

        private void Awake()
        {
            Debug.Assert(bulletRigidbody != null, "Rigidbody cannot be null");
            OnBulletSpawn?.Invoke();
        }

        public void Init()
        {
        }
        
        private void Update()
        {
            Move();
            
            if (destroyTime > 0)
            {
                destroyTime -= Time.deltaTime;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void Move()
        {
            bulletRigidbody.velocity = transform.forward * speed;
        }

        public float GetDamage()
        {
            return damage;
        }
        
        private void OnTriggerEnter(Collider other)
        {
            OnBulletHit?.Invoke();
            Instantiate(hitEffect,transform.position,Quaternion.identity);

            if (other.CompareTag("Enemy"))
            {
                var target = other.gameObject.GetComponent<EnemyDrone>();
                target.TakeHit(damage);
                Debug.Log($"Bullet hit enemy > Health : {target.Health}");
            }
            
            Destroy(gameObject);
        }
    }
}

