﻿using System;
using Manager;
using UnityEngine;

namespace Items.ItemCollection
{
    public class QuestItem : BaseItem, ICollectable
    {
        public event Action OnCollected;
        
        public override void Init(string itemName, float weight, float cost, Type type)
        {
            Name = itemName;
            Weight = weight;
            Cost = cost;
            itemType = type;
        }

        
        public void Collected()
        {
            OnCollected?.Invoke();
        }
    }
}